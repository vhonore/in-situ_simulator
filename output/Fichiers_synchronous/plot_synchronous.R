# Copyright (c) 2018-2019 Univ. Bordeaux


### Set your path to data files here ###
setwd("/home/val/Desktop/Thèse/depots_git_public/in-situ_simulator/output/Fichiers_synchronous")





























install.packages("extrafont",repos = "http://cran.us.r-project.org")
library(ggplot2)
library (reshape2)
options(digits=15)
### Set files path
library(extrafont)
#font_import()




list_of_algorithms <- list('Increasing Peak','Decreasing Peak','Increasing Time','Decreasing Time','Random','Optimal')










## Getting line argument for vertical space between two plots ##
args = commandArgs(trailingOnly=TRUE)
vspace = 0
if (length(args)!=1) 
   stop("At least one argument must be supplied in the makefile: the vertical space between the two plots", call.=FALSE)
t <- strsplit(args, '=')
 options(digits=10)
 vspace <- as.double(t[[1]][2])






### configuration.csv ###
# Contains the x axis labels + the different test sets #
config <-  scan("configuration.csv", what="", sep="\n")

memory_load_application <- list()
bandwidth <- list()
number_runs <- 0
memory_per_node <- 0
memory_simu <- 0
memory <- 0
work_simu <-0
nodes <- 0
cores <- 0


tmp <- unlist(strsplit(config[1], split=","))
for (values in tmp)
{
  memory_load_application <- c(memory_load_application,as.double(values))
}


tmp <- unlist(strsplit(config[2], split=","))
for (values in tmp)
{
  bandwidth <- c(bandwidth,as.double(values))
}

tmp <- unlist(strsplit(config[3], split=","))
memory_per_node <- as.double(tmp[1])

tmp <- unlist(strsplit(config[4], split=","))
number_runs <- as.double(tmp[1])

tmp <- unlist(strsplit(config[5], split=","))
memory_simu <- as.double(tmp[1])

tmp <- unlist(strsplit(config[6], split=","))
memory <- as.double(tmp[1])

tmp <- unlist(strsplit(config[7], split=","))
work_simu <- as.double(tmp[1])

tmp <- unlist(strsplit(config[8], split=","))
nodes <- as.double(tmp[1])

tmp <- unlist(strsplit(config[9], split=","))
cores <- as.double(tmp[1])

























## PLOTS EXEC TIME AND MEMORY IS OCCUPATION ###
# x = memory ; y = exec_time
# one plot containing the result of the algorithms is generated for each analysis data set

### get exec time ###

exec_time_synch <- list()
exec_time_synch_normalized <- list()
exec_time_asynch_normalized <- list()
exec_time_async <- list()

memoryLoadIS <- list()

for (j in 1:length(bandwidth))
{
  exec_time_synch[[j]] <- list()
  exec_time_async[[j]] <- list()
  exec_time_synch_normalized[[j]] <- list()
  exec_time_asynch_normalized[[j]] <- list()
  memoryLoadIS[[j]] <- list()
}


for (j in 1:length(bandwidth))
{
  e <- scan(paste("exec_time_",paste(j,".csv",sep=""),sep=""), what="", sep="\n")
  f <- scan (paste("../Fichiers_asynchronous/exec_time_",paste(j,".csv",sep=""),sep=""), what="", sep="\n")
  g <- scan(paste("memoryLoadIS_",paste(j,".csv",sep=""),sep=""), what="", sep="\n")
  
  for (i in 1:length(e))
  {
    tmp <- unlist(strsplit(e[i], split=","))
    tmpo <- unlist(strsplit(f[i], split=","))
    tmpoo <- unlist(strsplit(g[i], split=","))
    
    t <- list()
    u <- list()
    v <- list()
    
    for (c in 1:length(tmp))
    {
      # getting execution time
      t <- c(t,as.double(tmp[[c]]))
      z <- as.double(tmpoo[[c]])
      u <- c(u,((z/memory)/(memory_simu/memory)))
      v <-c(v,as.double(tmpo[[c]]))
      # getting analysis execution mode 

    }
    exec_time_synch[[j]][[i]] <- t
    memoryLoadIS[[j]][[i]] <- u
    exec_time_async[[j]][[i]] <- v
  }
  
  
  upb <- length(exec_time_synch[[j]])
  
  for (i in 1:upb)
  {
    tmp <- list()
    tmp2 <- list()
    for (k in 1:length(exec_time_synch[[j]][[i]]))
    {
      a<- exec_time_synch[[j]][[6]][[k]]
      b<- exec_time_async[[j]][[i]][[k]]
      c<- exec_time_synch[[j]][[i]][[k]]
      tmp <-c(tmp,as.double(as.double(b)/(as.double(a))))
      tmp2 <- c(tmp2,as.double(as.double(c)/as.double(a)))
    }
    exec_time_asynch_normalized[[j]][[i]] <- tmp
    exec_time_synch_normalized[[j]][[i]] <- tmp2
  }
}

### plot ###

for (j in 1:length(bandwidth))
{
  min_y <- +Inf
  max_y <- 0
  a <- +Inf
  b <- 0
  
  min_x <- min(unlist(memory_load_application))
  max_x <- max(unlist(memory_load_application))*1.01
  
  for (liste in exec_time_synch_normalized[[j]])
  {
    min_y <- min(min_y,min(unlist(liste)))
    max_y <- max(max_y,max(unlist(liste)))
  }
  
  for (liste in exec_time_asynch_normalized[[j]])
  {
    a <- min(min_y,min(unlist(liste)))
    b <- max(max_y,max(unlist(liste)))
  }
  
  min_y <- min(min_y,a)*0.95
  max_y <- max(max_y,b)*1.05
  
  
  
  pdf(file = paste(paste("plot_comparison_AS_S",j,sep=""),".pdf",sep=""), onefile=TRUE,
      width=8.27, height=10, paper="a4r")
  
  list_of_algorithms <- list('Decreasing Peak AS','Decreasing Peak S','Increasing Time AS','Increasing Time S','Optimal AS')
  

  plot(memory_load_application, exec_time_synch_normalized[[j]][[1]], type = "n", xlim = c(min_x,max_x), ylim = c(min_y, max_y), xlab = "Memory Load of Application (Simulation = 1 unit, Total memory = 1.33 units)", ylab = "Execution time (normalized with synchronous optimal)")
  lines(memory_load_application, exec_time_asynch_normalized[[j]][[1]], col = "dodgerblue2",type="b", pch=0)
  lines(memory_load_application, exec_time_synch_normalized[[j]][[1]], col = "dodgerblue2",type="b", pch=6)
  lines(memory_load_application,exec_time_asynch_normalized[[j]][[3]] , col = "red",type="b", pch=3)
  lines(memory_load_application,exec_time_synch_normalized[[j]][[3]] , col = "red",type="b", pch=7)
  lines(memory_load_application,exec_time_asynch_normalized[[j]][[6]] , col = "purple",type="b", pch=5)
  abline(h=1, col="black",lty=2,lwd=2)
  legend(x="topright",legend=c(list_of_algorithms[1],list_of_algorithms[2],list_of_algorithms[3],list_of_algorithms[4],list_of_algorithms[5]),text.col=c("dodgerblue2","dodgerblue2","red","red","purple"),col=c("dodgerblue2","dodgerblue2","red","red","purple"),cex=1.4,title="Algorithms",title.col = "black",pch=c(0,6,3,7,5))
  
  

  
  dev.off()
  ## Embed all fonts ##
  x= paste(paste(".pdf -f plot_comparison_AS_S",j,sep=""),".pdf",sep="")
  k = paste(paste("plot_comparison_AS_S_embedded",j,sep=""),x,sep="")   
  c = paste("gs -dSAFER -dNOPLATFONTS -dNOPAUSE -dBATCH -sDEVICE=pdfwrite -sPAPERSIZE=letter -dCompatibilityLevel=1.4 -dPDFSETTINGS=/printer -dCompatibilityLevel=1.4 -dMaxSubsetPct=100 -dSubsetFonts=true -dEmbedAllFonts=true -sOutputFile=",k,sep="")
  system(command = c)
}

